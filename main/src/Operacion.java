/**
 * Created by Erick on 04/06/2017.
 */
public class Operacion {

    void operacion(int entrada){

        System.out.println("\n \n");
        operadoresBitwise(entrada);

        System.out.println("\n \n");
        operadoresBinarios(entrada);

        System.out.println("\n \n");
        int num1 = -entrada;
        System.out.println(num1);
        int num2 = +entrada;
        System.out.println(num2);
        int num3 = ~entrada;
        System.out.println(num3);



    }

    void operadoresBitwise(int entrada){
        int num = 14;
        System.out.println("\n \n");
        int opAND = entrada & num;
        System.out.println(opAND);
        int opOR = entrada | num ;
        System.out.println(opOR);
        int opORExc = entrada ^ num;
        System.out.println(opORExc);

        double salida = (double)opAND * (double)opOR / (double)opORExc;
    }

    void operadoresBinarios(int num1){

        boolean bool = true;
        boolean bool1 = false;

        for (int i=0 ; i<num1; i++){

            if(i%2 == 0 && i > 0) {
                for (int j = i; j >= 0; j--) {
                    System.out.print("*");
                }
            }
            System.out.println("");
        }

        System.out.println("\n \n ");
        if (bool & bool1 || bool | bool1 || bool ^ bool1) {
            for (int i = 0; i <= num1; i++) {

                if (i % 2 != 0 || i % 4 != 0) {
                    for (int j = i; j > 0; j--) {
                        System.out.print("*");
                    }
                }
                System.out.println("");
            }
        }
    }
}