/**
 * Created by Erick on 04/06/2017.
 */
public class Casting {

    void convertir(byte entrada){

        System.out.println("El numero fue convertido del Byte: " + entrada + " a: ");
        short num1 = (short)entrada;
        System.out.println("Short es igual a: " + num1);
        int num2 = (int)num1;
        System.out.println("Int es igual a: " + num2);
        long num3 = (long)num2;
        System.out.println("Long es igual a: " + num3);
        float num4 = (float)num3;
        System.out.println("Float es igual a: " + num4);
        double num5 = (double)num4;
        System.out.println("Double es igual a: " + num5);

    }
}
