/**
 * Created by Erick on 04/06/2017.
 */
public class Conversion {

    void convertir(byte entrada){

        System.out.println("El numero fue convertido del Byte: " + entrada + " a: ");
        short num1 = conversionByteShort(entrada);
        System.out.println("Short es igual a: " + num1);
        int num2 = conversionShortInt(num1);
        System.out.println("Int es igual a: " + num2);
        long num3 = conversionIntLong(num2);
        System.out.println("Long es igual a: " + num3);
        float num4 = conversionLongFloat(num3);
        System.out.println("Float es igual a: " + num4);
        double num5 = conversionFloatDouble(num4);
        System.out.println("Double es igual a: " + num5);
    }

    short conversionByteShort (byte entrada){
        short result = entrada;
        return result;
    }

    int conversionShortInt (short entrada){
        int result = entrada;
        return result;
    }

    long conversionIntLong(int entrada){
        long result = entrada;
        return result;
    }

    float conversionLongFloat(long entrada){
        float result = entrada;
        return result;
    }

    double conversionFloatDouble(float entrada){
        double result = entrada;
        return result;
    }

}
